You'll find some of the most frequently asked questions about the
ShareLatex installation and its specificity.

- The service is hosted here: [https://sharelatex.irisa.fr](https://sharelatex.irisa.fr)

- For a more general introduction to ShareLatex please refer to the official
  documentation :
  [https://www.sharelatex.com/learn/](https://www.sharelatex.com/learn/)

- Support & questions can be sent to 
  - [support-sharelatex@inria.fr](mailto:sharelatex-support@inria.fr)
  - or on our mattermost channel: https://mattermost.inria.fr/sharelatex
 
- Frequently asked questions: [FAQ](faq.md)

- We try to keep an up-to-date events list that concerns the service here: [Events](events.md)



