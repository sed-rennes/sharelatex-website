# Migration notes from https://sharelatex.irisa.fr to https://overleaf.irisa.fr

Migration planning:

- **1st January 2023**: https://sharelatex.irisa.fr will be read-only
- **31 December 2023**: https://sharelatex.irisa.fr will be shutdown

In the meantime we encourage you to use https://overleaf.irisa.fr

##  How to authenticate to https://overleaf.irisa.fr ? 


Authentication now relies on https://gitlab.inria.fr OAUTH mechanism. You need to have an account on Gitlab and accept the Overleaf application.

- If you have an Inria LDAP account, you should be able to connect to https://overleaf.irisa.fr using this account.
![](figs/ldap.png)

- If you are an external collaborator, you'll need a standard account on https://gitlab.inria.fr first. Please refer to this page: https://gitlab.inria.fr/siteadmin/doc/-/wikis/home#gitlab-accounts

![](figs/standard.png)

## How to migrate a project from https://sharelatex.irisa.fr to https://overleaf.irisa.fr

- via web interfaces:
    - create a project zip file and download it at project menu in https://sharelatex.irisa.fr using "Source" button
    
    ![](figs/download.png)

    - create a new project in https://overleaf.irisa.fr with `upload project`: 
    
    ![](figs/upload.png)

- with [`python-sharelatex>=1.0.0`](https://gitlab.inria.fr/sed-rennes/sharelatex/python-sharelatex) in command line:

1. clone the project with project URL : 
    
```bash
git slatex clone https://sharelatex.irisa.fr/project/{project_UUID} my_directory
# use "legacy" auth method
```

2. create a project by uploading the local copy:

```bash
cd my_directory
git slatex new my_project https://overleaf.irisa.fr
# use "gitlab" auth method
```
