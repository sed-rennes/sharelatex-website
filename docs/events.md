# In progress

* Since 14. August 2022 the service is unstable due to a power outage in Rennes site

---

# Planned


---
# Done
* **11. September 2020**
    * Upgrade compile environment to texlive 2020.
    * Fix lualatex support
* **14. January 2020**
    * Add `ens-rennes.fr` to the priviledged domain list.
* **From 19. January 2019 1am to 19. January 2019 1am:** system + service upgrade -- service unavalaible
    * Base system has been upraded to Ubuntu18.04
    * Sharelatex is now upgraded to version 1.2.1 (latest published version
      of the community edition)
    * New FAQ is available here : https://sed-rennes.gitlabpages.inria.fr/sharelatex-website/
