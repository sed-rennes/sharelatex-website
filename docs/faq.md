# FAQ

---

## How do I login ?

You need to register first.
Sharelatex accounts are local accounts that are not tied to Inria Single Sign On
mecanism.

## I can't create a project

Only user with email in the following domains are allowed to create a project :

* ```inria.fr``` ```irisa.fr``` ```loria.fr``` ```inrialpes.fr``` ```inria.cl``` ```ens-rennes.fr```

---

## The service is unavailable

This shouldn't happen except for the daily backups. Contact us if you are suspecting an unappropriate disruption.

---

## I can't upload my project

Request size to the ShareLatex instance is limited to 100 MB.
Please check the size of your project before uploading it.

---

## The produced PDF can't be read by my local PDF viewer

There are several PDF format and in some situation some viewer expect a specific PDF format version.
By default Sharelatex will produce PDF in version 1.5.
Nevertheless you can set the PDF compiler target format using the `
\pdfminorversion` command in your source code.

```
# set the target format to 1.6
\pdfminorversion=6
```

Reference: http://mirrors.standaloneinstaller.com/ctan/systems/doc/pdftex/manual/pdftex-a.pdf


## I can't share my project with others collaborators

Button "share" is missing for projects who you aren't owner, because only project's owner is allowed to share it.
But if you are the owner of your project and button "share" is missing,  please check if you don't have any "blocker" (adblock, ublouk, or shield) active in your web browser. 

---

## Service interruption

The service is offline twice a day at 1:10 am/pm to perform a backup. 
This shouldn't last more than 5 minutes.

As a rule of thumb and as long ShareLatex@Irisa is in beta version,
we encourage you to keep a local backup of your work.

---

## Can I get my project synchronized with Git ?

Yes, We have developed [**Python-sharelatex**](https://pypi.org/project/sharelatex/)

Python-sharelatex is a library to interact with https://sharelatex.irisa.fr. It
also includes a command line tools to sync your remote project with Git. This
allows you to work offline on your project and later sync your local copy with
the remote one.

See [https://pypi.org/project/sharelatex/](https://pypi.org/project/sharelatex/) for more details 


---

## Automatic backup using Gitlab CI

- clone a remote sharelatex project and push it back to the current repository
- run on schedule only
- require some variables 
    + `SHARELATEX_XXX` for accesssing sharelatex
    + `BACKUP_PASSWORD` is an access token (Settings > Repository > Access Token)

```yaml
backup:
    tags:
    - ci.inria.fr
    only:
    - schedules
    image: python:3.7-buster
    variables:
        PROJECT: 5cdd49d92ef2085508de8f02
    script:
    - apt update && apt install -y rsync
    - pip install sharelatex
    - pushd /tmp
    - git slatex clone https://overleaf.irisa.fr/project/${PROJECT} -u ${SHARELATEX_USER} -p ${SHARELATEX_PASSWORD}
    - popd
    - rsync -avz --delete --exclude .git /tmp/${PROJECT}/ .
    - rm -rf ${PROJECT}
    # prepare to push back in the repo
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - git remote set-url origin https://${BACKUP_USER}:${BACKUP_PASSWORD}@gitlab.inria.fr/${CI_PROJECT_PATH}.git
    - git checkout .gitlab-ci.yml
    - git add .
    - git diff-index HEAD --exit-code || git commit -m 'backup'
    - git push origin "HEAD:${CI_COMMIT_BRANCH}"
```

## Getting a zip of a project programmaticaly

You want to have a look at [**Python-sharelatex**](https://pypi.org/project/sharelatex/).

Alternatively, the following python script download the zip of a given project.

Note that it is given for illustration purpose and it's maybe better to not 
hardcode the password in the file.

```python
import re
import requests


BASE_URL = "https://sharelatex.irisa.fr"
LOGIN_URL = "{}/login".format(BASE_URL)

email = "YOUR_LOGIN"
password = "YOUR_PASSWORD"
project_id = "PROJECT_ID"

zip_url = "{base}/project/{pid}/download/zip".format(base=BASE_URL,
                                                     pid=project_id)

client = requests.session()

# Retrieve the CSRF token first
r = client.get(LOGIN_URL, verify=False)
csrftoken = re.search('(?<=csrfToken = ").{36}', r.text).group(0)

# login
login_data = {"email": email,
              "password":password,
              "_csrf":csrftoken}

r = client.post(LOGIN_URL, data=login_data, verify=False)
r = client.get(zip_url, stream=True)

with open("{}.zip".format(project_id), 'wb') as f:
    for chunk in r.iter_content(chunk_size=1024):
        if chunk:
            f.write(chunk)
```

## Feature X exists in the official sharelatex/overleaf website but not in our deployment

It's likely because the feature X is part of the enterprise edition.

---

## Compilation


### Latex distribution

The distribution available for compiling is a texlive scheme-full.


### Shell-escape

`-shell-escape` is set for the compilation. As a consequence some package like `minted` can be used.

### Dot2tex


`dot2texi` is available, the following will be display a graph in the output :

```latex
\documentclass{article}
\usepackage{dot2texi}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}

\begin{document}

\begin{dot2tex}[neato,options=-tmath]
digraph G {
    node [shape="circle"];
    a_1 -> a_2 -> a_3 -> a_4 -> a_1;
}
\end{dot2tex}

\end{document}
```

### "Timed out"

Your compilation takes more than two minutes and has been killed to free
resources on the server.  This may be due to a large number of high-res images,
complicated diagrams. We also observed that specific syntax errors can produce
this behaviour (e.g syntax errors in array environment)

Note that you can compile in "draft mode". It can be used to lighten the 
compilation process.

---

## Where can I get support ?

Feel free to join: [https://mattermost.inria.fr/sharelatex/channels/users-help](https://mattermost.inria.fr/sharelatex/channels/users-help)

Or if you can't access it, you can still write us an email at
[support-sharelatex@inria.fr](mailto:support-sharelatex@inria.fr)
